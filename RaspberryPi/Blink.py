import ptvsd
ptvsd.enable_attach(secret = 'pi', address = ('0.0.0.0', 8080))
import RPi.GPIO as GPIO
import time

# Use physical pin numbers
GPIO.setmode(GPIO.BCM)
# Set up header pin 11 (GPIO17) as an input
print "Setup Pin 26"
GPIO.setup(7, GPIO.OUT)
print "Setup Pin 10"
GPIO.setup(21, GPIO.OUT)

var=1
print "Start loop"
while var==1 :
  print "Set Output False"
  GPIO.output(7, False)
  GPIO.output(21, True)
  time.sleep(0.5)
  print "Set Output True"
  GPIO.output(7, True)
  GPIO.output(21, False)
  time.sleep(0.5)
  print "success"

